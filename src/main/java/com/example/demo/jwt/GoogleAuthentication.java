package com.example.demo.jwt;

import com.example.demo.service.UserPrinciple;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class GoogleAuthentication extends AbstractAuthenticationToken {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8254831403638075928L;

    private UserPrinciple registeredUser;

    public GoogleAuthentication(
            Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    /**
     * @param registeredUser
     */
    public GoogleAuthentication(UserPrinciple registeredUser) {
        super(null);
        this.registeredUser = registeredUser;
    }

    @Override
    public Object getCredentials() {
        return "NOT_REQUIRED";
    }

    @Override
    public Object getPrincipal() {
        return registeredUser;
    }

    /**
     * @return the registeredUser
     */
    public UserPrinciple getUserDetail() {
        return registeredUser;
    }

    /**
     * @param registeredUser
     *            the registeredUser to set
     */
    public void setUserDetail(UserPrinciple registeredUser) {
        this.registeredUser = registeredUser;
        setDetails(registeredUser);
    }
}
