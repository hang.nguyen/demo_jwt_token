package com.example.demo.service;

import com.example.demo.model.GoogleAuthForm;
import com.example.demo.model.JwtResponse;

public interface AuthService {

//    JsonResponse registerUser(SignUpForm signUpRequest);

    JwtResponse authenticateWithGoogle(GoogleAuthForm googleAuthForm);
}
