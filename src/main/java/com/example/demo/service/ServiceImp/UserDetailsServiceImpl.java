package com.example.demo.service.ServiceImp;

import com.example.demo.constant.MessageConstants;
import com.example.demo.constant.RoleName;
import com.example.demo.entity.RoleEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.exception.InvalidTokenException;
import com.example.demo.jwt.JwtProvider;
import com.example.demo.model.GoogleAuthForm;
import com.example.demo.model.JwtResponse;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.AuthService;
import com.example.demo.service.UserPrinciple;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;


import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Optional;

import static com.example.demo.constant.DummyData.DUMMY_PASSWORD;
import static com.example.demo.constant.ParameterConstants.CLIENT_ID;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, AuthService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    PasswordEncoder encoder;


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException(MessageConstants.USER_NOT_FOUND_WITH_USERNAME_OR_PASSWORD + " : " + username)
                );

        return UserPrinciple.build(user);
    }

//    @Override
//    public JwtResponse authenticateUser(LoginForm loginRequest) {
//        if (!ValidationBusinessUtil.isValidateUserName(loginRequest.getUsername())) {
//            throw new InvalidParamException(MessageConstants.INVALID_NAME);
//        }
//
//        if (!ValidationBusinessUtil.isValidatePassword(loginRequest.getPassword())) {
//            throw new InvalidParamException(MessageConstants.INVALID_PASSWORD);
//        }
//
//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        loginRequest.getUsername(),
//                        loginRequest.getPassword()
//                )
//        );
//        return generateJwtTokenAndRefreshToken(authentication);
//    }

//    @Override
//    public JsonResponse registerUser(SignUpForm signUpRequest) {
//
//        if (ValidationBusinessUtil.isEmptyObject(signUpRequest.getUsername()) || ValidationBusinessUtil.isEmptyObject(signUpRequest.getPassword())) {
//            throw new InvalidParamException(MessageConstants.EMPTY_USERNAME_OR_PASSWORD);
//        }
//
//        if (!ValidationBusinessUtil.isValidateUserName(signUpRequest.getUsername())) {
//            throw new InvalidParamException(MessageConstants.INVALID_NAME);
//        }
//
//        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
//            throw new InvalidParamException(MessageConstants.USER_ALREADY_TAKEN);
//        }
//
//        if (!ValidationBusinessUtil.isValidatePassword(signUpRequest.getPassword())) {
//            throw new InvalidParamException(MessageConstants.INVALID_PASSWORD);
//        }
//
//        if (!ValidationBusinessUtil.isValidateEmail(signUpRequest.getEmail())) {
//            throw new InvalidParamException(MessageConstants.INVALID_EMAIL);
//        }
//
//        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
//            throw new InvalidParamException(MessageConstants.EMAIL_ALREADY_USED);
//        }
//
//        UserEntity user = new UserEntity(signUpRequest.getUsername(),
//                signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));
//
//        Set<String> strRoles = signUpRequest.getRole();
//
//        Set<RoleEntity> roles = new HashSet<>();
//
//        strRoles.forEach(role -> {
//            switch (role) {
//                case RoleConstants.ROLE_CHARITY:
//                    RoleEntity charityRole = roleRepository.findByName(RoleName.ROLE_CHARITY)
//                            .orElseThrow(() -> new RuntimeException(MessageConstants.ROLE_NOT_FOUND));
//                    roles.add(charityRole);
//
//                    break;
//                default:
//                    RoleEntity donorRole = roleRepository.findByName(RoleName.ROLE_DONOR)
//                            .orElseThrow(() -> new RuntimeException(MessageConstants.ROLE_NOT_FOUND));
//                    roles.add(donorRole);
//            }
//        });
//
//        user.setRoles(roles);
//        userRepository.save(user);
//        DonatorEntity donatorEntity = new DonatorEntity();
//        donatorEntity.setUserEntity(user);
//        donatorEntity.setEmail(user.getEmail());
//        donatorEntity.setAvatarUrl(DummyData.AVATAR_DEFAULT_IMAGE);
//        donatorRepository.save(donatorEntity);
//        return new JsonResponse(MessageConstants.REGISTER_SUCCESS);
//    }

    @Override
    public JwtResponse authenticateWithGoogle(GoogleAuthForm googleAuthForm) {
        try {
        NetHttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
        JacksonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList(CLIENT_ID))
                .build();

        GoogleIdToken idToken = Optional.ofNullable(verifier.verify(googleAuthForm.getIdToken()))
                                        .orElseThrow(() -> new InvalidTokenException(MessageConstants.INVALID_TOKEN));

        GoogleIdToken.Payload payload = idToken.getPayload();
        String userId = payload.getSubject();
        String email = payload.getEmail();
        String name = (String) payload.get("name");
        String pictureUrl = (String) payload.get("picture");

        if (!userRepository.existsByEmail(email)) {
            RoleEntity roleEntity = roleRepository.findByName(RoleName.ROLE_USER)
                    .orElseThrow(() -> new EntityNotFoundException(MessageConstants.ROLE_NOT_FOUND));
            UserEntity user = new UserEntity(email, email, encoder.encode(DUMMY_PASSWORD));
            user.setRoles(Collections.singleton(roleEntity));
            userRepository.save(user);
        }


        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, DUMMY_PASSWORD));

        return generateJwtTokenAndRefreshToken(authentication);


        } catch (GeneralSecurityException | IOException | AuthenticationException e) {
            e.printStackTrace();
            throw new InvalidTokenException(MessageConstants.INVALID_TOKEN);
        }
    }

    private JwtResponse generateJwtTokenAndRefreshToken(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateJwtToken(authentication);
        return new JwtResponse(jwt, MessageConstants.ResultCode.SUCCESS, MessageConstants.SUCCESS);
    }
}