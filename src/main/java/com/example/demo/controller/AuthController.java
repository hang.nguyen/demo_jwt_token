package com.example.demo.controller;


import com.example.demo.constant.PathConstants;
import com.example.demo.jwt.JwtProvider;
import com.example.demo.model.GoogleAuthForm;
import com.example.demo.model.JwtResponse;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping(PathConstants.AUTH_PATH)
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthService authService;

    @PostMapping(PathConstants.GOOGLE)
    public ResponseEntity<Object> authenticateWithGoogle(@Valid @RequestBody GoogleAuthForm googleAuthForm) {
        return new ResponseEntity<>(authService.authenticateWithGoogle(googleAuthForm), HttpStatus.OK);
    }

}