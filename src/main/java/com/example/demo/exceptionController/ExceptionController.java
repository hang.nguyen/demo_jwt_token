package com.example.demo.exceptionController;

import com.example.demo.model.ExceptionResponse;
import com.example.demo.exception.*;
import com.example.demo.model.ErrorDetails;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import com.example.demo.exception.EntityNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@CrossOrigin
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity handleEntityNotFoundException(EntityNotFoundException ex) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                ex.getErrorMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidTokenException.class)
    public ResponseEntity handleInvalidTokenException(InvalidTokenException ex) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                ex.getErrorMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(InvalidParamException.class)
    public ResponseEntity handleInvalidParamException(InvalidParamException ex) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                ex.getErrorMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AuthorizationFailException.class)
    public ResponseEntity handleAuthorizationFailException(AuthorizationFailException ex) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                ex.getErrorMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(InvalidImageSizeException.class)
    public ResponseEntity handleInvalidImageSizeException(InvalidImageSizeException ex) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                ex.getErrorMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(ReviewExistedException.class)
    public ResponseEntity handleReviewExistedException(ReviewExistedException ex) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                ex.getErrorMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
    }



    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(ex.getBindingResult().getAllErrors().get(0).getDefaultMessage(), "Validation Failed");
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }
}