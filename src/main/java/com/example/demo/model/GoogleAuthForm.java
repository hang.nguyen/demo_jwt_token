package com.example.demo.model;

import javax.validation.constraints.NotNull;

public class GoogleAuthForm {

    @NotNull(message = "idToken must not be null")
    private String idToken;

    public GoogleAuthForm() {
    }

    public GoogleAuthForm(@NotNull(message = "idToken must not be null") String idToken) {
        this.idToken = idToken;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }
}