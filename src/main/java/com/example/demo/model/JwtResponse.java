package com.example.demo.model;

public class JwtResponse {
    private String accessToken;
    private int resultCode;
    private String resultMessage;

    public JwtResponse(String accessToken, int resultCode, String resultMessage) {
        this.accessToken = accessToken;
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

}