package com.example.demo.exception;

public class AuthorizationFailException extends RuntimeException {
    private String errorMessage;

    public AuthorizationFailException(String message) {
        super(message);
        this.errorMessage = message;
    }

    public AuthorizationFailException() {}

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
