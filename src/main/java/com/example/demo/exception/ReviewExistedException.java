package com.example.demo.exception;

public class ReviewExistedException  extends RuntimeException{
    private String errorMessage;

    public ReviewExistedException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
