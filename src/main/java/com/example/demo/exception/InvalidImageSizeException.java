package com.example.demo.exception;

public class InvalidImageSizeException extends RuntimeException{
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public InvalidImageSizeException(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
