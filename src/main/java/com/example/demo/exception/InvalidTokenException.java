package com.example.demo.exception;

public class InvalidTokenException extends RuntimeException{
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public InvalidTokenException(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
