package com.example.demo.constant;

public class MessageConstants {
    public static final String CHARITY_NOT_FOUND = "Charity not found!";
    public static final String DONATOR_NOT_FOUND = "Donator not found!";
    public static final String USER_ALREADY_TAKEN = "Username is already taken";
    public static final String EMAIL_ALREADY_USED =  "Email is already used";
    public static final String EMPTY_EMAIL = "Email is empty";
    public static final String EMPTY_NAME = "Name is empty";
    public static final String INVALID_NAME = "Invalid name";
    public static final String INVALID_PASSWORD = "Invalid password";

    public static final String INVALID_USERNAME_OR_PASSWORD = "Invalid username or password";
    public static final String EMPTY_USERNAME_OR_PASSWORD = "Empty username or password";
    public static final String ROLE_NOT_FOUND = "Role is not found";
    public static final String UNAUTHORIZED = "Unauthorized Error";
    public static final String USER_NOT_FOUND_WITH_USERNAME_OR_PASSWORD = "We were not able to find a user with that username and password";
    public static final String USER_NOT_FOUND_BY_ID_USER = "User was not found by id user";

    public static final String INVALID_LENGTH_NAME = "Invalid length name";
    public static final String INVALID_LENGTH_PASSWORD= "Invalid length password";
    public static final String INVALID_EMAIL = "Invalid email";
    public static final String INVALID_PHONE = "Invalid phone";
    public static final String INVALID_SIZE_IMAGE = "Invalid image size";

    public static final String REGISTER_SUCCESS = "User registered successfully";
    public static final String REGISTER_FAIL = "Something was wrong when registered user account";
    public static final String SUCCESS = "Success";
    public static final String FAIL = "Something was wrong";
    public static final String INVALID_TOKEN = "Invalid Token";
    public static final String USER_NOT_FOUND_WITH_USERNAME = "We were not able to find a user with that username.";

    public static final String CREATE_CHARITY_SUCCESS = "Create a charity successfully";
    public static final String UPDATE_CHARITY_SUCCESS = "Update charity successfully";
    public static final String UPLOAD_AVATAR_SUCCESS = "Upload avatar successfully";
    public static final String UPLOAD_COVER_SUCCESS = "Upload cover successfully";
    public static final String CREATE_CHARITY_FAIL = "Something error when created charity";
    public static final String UPDATE_CHARITY_FAIL = "Something error when updated charity";

    public static final String CHARITY_NOT_FOUND_WITH_ID_CHARITY_AND_ID_USER = "Charity was not found with id_charity and id_user";
    public static final String UPDATE_GALLERY_SUCCESS = "Update gallery successfully";
    public static final String NOT_NULL = "can not be null";
    public static final String REVIEW_FOUND = "Current user has not posted any review about the charity";
    public static final String REVIEW_EXISTED = "Review has existed";
    public static final String RATING_NOT_FOUND = "Rating not found";

    public static final String DELETE_IMAGE_SUCCESS = "Delete image in gallery successfully";
    public static final String NO_IMAGE_IN_GALLERY = "Delete image in gallery fail, you don't have any image in gallery";
    public static final String NO_IMAGE_REQUEST = "You haven't chosen any image yet!";
    public static final String FILE_NOT_FOUND = "File not found";

    public class ResultCode {
        public static final int FAIL = 400;
        public static final int SUCCESS = 200;
        public static final int UNAUTHORIZED = 401;
        public static final int FORBIDDEN = 403;
        public static final int NOT_FOUND = 404;
    }
}