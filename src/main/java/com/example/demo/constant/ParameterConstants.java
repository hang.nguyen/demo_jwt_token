package com.example.demo.constant;

public class ParameterConstants {
    public static final int PAGINATION_MAX_SIZE = 6;
    public static final String CLIENT_ID = "378077348336-ccqia65e1g2rgbn9shj0j3p9o321ifkc.apps.googleusercontent.com";
}
