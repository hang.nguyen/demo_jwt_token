package com.example.demo.constant;

public class PathConstants {

    /**
     * Base Path
     */
    public static final String AUTH_PATH = "/api/auth";

    /**
     * Path Parameter
     */
    public static final String ID = "/{id}";

    /**
     * Home path
     */
    public static final String GOOGLE = "/google";
}