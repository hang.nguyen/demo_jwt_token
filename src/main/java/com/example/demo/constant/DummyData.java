package com.example.demo.constant;

public class DummyData {
    public static final String DUMMY_PASSWORD = "dummypassword_secret";
    public static final String AVATAR_DEFAULT_IMAGE = "image154831172458267.jpg";
    public static final String COVER_DEFAULT_IMAGE = "image15474631702582.jpg";
}
